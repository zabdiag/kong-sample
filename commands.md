### Create docker network

```bash
 $ docker network create kong-net
```

### Start database

```bash
 $ docker run -d --name kong-database \
               --network=kong-net \
               -p 5432:5432 \
               -e "POSTGRES_USER=kong" \
               -e "POSTGRES_DB=kong" \
               -e "POSTGRES_PASSWORD=kong" \
               postgres:9.6
```


### Migrations
```bash
 $ docker run --rm \
     --network=kong-net \
     -e "KONG_DATABASE=postgres" \
     -e "KONG_PG_HOST=kong-database" \
     -e "KONG_PG_USER=kong" \
     -e "KONG_PG_PASSWORD=kong" \
     -e "KONG_CASSANDRA_CONTACT_POINTS=kong-database" \
     kong:latest kong migrations bootstrap
```

### start kong
```bash
 $ docker run -d --name kong \
     --network=kong-net \
     -e "KONG_DATABASE=postgres" \
     -e "KONG_PG_HOST=kong-database" \
     -e "KONG_PG_USER=kong" \
     -e "KONG_PG_PASSWORD=kong" \
     -e "KONG_CASSANDRA_CONTACT_POINTS=kong-database" \
     -e "KONG_PROXY_ACCESS_LOG=/dev/stdout" \
     -e "KONG_ADMIN_ACCESS_LOG=/dev/stdout" \
     -e "KONG_PROXY_ERROR_LOG=/dev/stderr" \
     -e "KONG_ADMIN_ERROR_LOG=/dev/stderr" \
     -e "KONG_ADMIN_LISTEN=0.0.0.0:8001, 0.0.0.0:8444 ssl" \
     -p 8000:8000 \
     -p 8443:8443 \
     -p 127.0.0.1:8001:8001 \
     -p 127.0.0.1:8444:8444 \
     kong:latest
```


### TO run konga, a web app kong admin
```bash
$ docker pull pantsel/konga
$ docker run -p 1337:1337 \
             --network kong-net \
             --name konga \
             -e "NODE_ENV=production" \
             -e "TOKEN_SECRET=randomsecret" \
             pantsel/konga
```


### Build and run users service
```bash
$ docker build -t luisag/users users/
$ docker run --rm --network=kong-net --name users luisag/users
```

### Build and run plans service
```bash
$ docker build -t luisag/plans plans/
$ docker run --rm --network=kong-net --name plans luisag/plans
```


### Create users-service, plans-service and routes on kong
```bash
curl -i -X POST \
  --url http://localhost:8001/services/ \
  --data 'name=users-service' \
  --data 'url=http://users'


# OUTPUT
{
    "client_certificate": null,
    "connect_timeout": 60000,
    "created_at": 1593993842,
    "host": "users",
    "id": "a33cd8a4-06bd-4f1a-b157-7de18bd0bc96",
    "name": "users-service",
    "path": null,
    "port": 80,
    "protocol": "http",
    "read_timeout": 60000,
    "retries": 5,
    "tags": null,
    "updated_at": 1593993842,
    "write_timeout": 60000
}

curl -i -X POST \
  --url http://localhost:8001/services/users-service/routes \
  --data 'hosts[]=example.com'


curl -i -X POST \
  --url http://localhost:8001/services/ \
  --data 'name=plans-service' \
  --data 'url=http://plans'

{"host":"plans","created_at":1594039045,"connect_timeout":60000,"id":"ccd7c46c-3181-4ef9-9f90-80ec5babf650","protocol":"http","name":"plans-service","read_timeout":60000,"port":80,"path":null,"updated_at":1594039045,"retries":5,"write_timeout":60000,"tags":null,"client_certificate":null}

curl -i -X POST http://localhost:8001/services/plans-service/routes \
  --data 'paths[]=/plans' \
  --data 'name=plans'

{"id":"7972e09e-1d85-4cd7-8415-5382c578760f","path_handling":"v0","paths":["\/plans"],"destinations":null,"headers":null,"protocols":["http","https"],"methods":null,"snis":null,"service":{"id":"ccd7c46c-3181-4ef9-9f90-80ec5babf650"},"name":"plans","strip_path":true,"preserve_host":false,"regex_priority":0,"updated_at":1594039960,"sources":null,"hosts":null,"https_redirect_status_code":426,"tags":null,"created_at":1594039960}
```


### some basic commands to consume API Gateway

```bash
curl -i -X GET http://localhost:8000/plans

curl -i -X GET http://localhost:8000/plans/custom-route
curl -i -X POST http://localhost:8000/plans/custom-route \
  -H "Content-Type: application/json" \
 --data '{"here":"iam"}'

```
