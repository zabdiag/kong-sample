'use strict';

const express = require('express');

// Constants
const PORT = 80;
const HOST = '0.0.0.0';

// App
const app = express();

app.use(express.json()) // for parsing application/json

app.get('/', (req, res) => {
  res.send('Hello World on plans');
});

app.get('/custom-route', (req, res) => {
  console.log('/custom-route GET')
  console.log('req', req);
  res.send('this is a custom route, GET');
});
app.post('/custom-route', (req, res) => {
  console.log('/custom-route POST')
  console.log('req', req.body);
  res.send('this is a custom route');
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
